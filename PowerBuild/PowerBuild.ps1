﻿#
# Script.ps1
#
param (
	[bool]$nuget = $false,
    [bool]$selfExtracting = $true,
    [string]$dstDir = [Environment]::ExpandEnvironmentVariables("%IONTO_INST%") + '\OntologyManagement\'
)


cd "$PSScriptRoot\..\.."

$tempDir = [Environment]::ExpandEnvironmentVariables("%IONTO_INST%")
$sourceDir = (Get-Location)
$configuration = "Release"


[System.Reflection.Assembly]::LoadFrom('C:\Gitlab\NugetPackageLib\NugetPackageLib\bin\Debug\NugetPackageLib.dll');

if ($sourceDir.EndsWith("\"))
{
    $sourceDir = $sourceDir.Substring(0,$sourceDir.Length-1)
}


if (!(Test-Path -Path $dstDir)) 
{
    Write-Host "Create Folder $dstDir"
    New-Item -ItemType directory -Path "$dstDir"
}


Write-Host "Copy binaries to Folder: $sourceDir -> $dstDir (bin\$configuration)"
Get-ChildItem -Recurse $sourceDir -File | ?{ $_.FullName.Contains("AssemblyInfo.cs") -or $_.FullName.Contains("AssemblyInfo.vb")} |  % { 
    

    $version = %{select-string -Path $_.FullName -Pattern '^(<|\[)Assembly: AssemblyVersion\(.*\)' -AllMatches | % { $_.Matches } | % { $_.Value }}
    $version = %{select-string -InputObject $version -Pattern '\d+\.\d+\.\d+\.\d+' -AllMatches | % { $_.Matches } | % { $_.Value }}

    $projectFolder = $_.FullName.Replace("Properties\AssemblyInfo.cs","")
    $projectFolder = $projectFolder.Replace("My Project\AssemblyInfo.vb","")

    Get-ChildItem -Recurse $projectFolder -Directory | ?{ $_.FullName.EndsWith("bin\$configuration")} |  % { 
        $project = $_.FullName.Replace($sourceDir,"")
        $project = $project.Replace("\bin\$configuration","")
        $splitProject = $project.Split("\")
        $project = $splitProject[1]
        $destDir = $dstDir + $project
    
        $sourceFolder = $_.FullName
        & RoboCopy /MIR "$sourceFolder" "$destDir"

        $versionFilePath = $destDir + "\Version.txt"
        $version > $versionFilePath

        

		if ($nuget -eq $true)
		{
			$nugetId = $project -replace "-",""
			$test = New-Object NugetPackageLib.PackageCreator($version, $nugetId, "Tassilo Koller", $project)
			$test.AddFiles($destDir, $destDir, $nugetId)
			$test.SaveSpec("$tempDir\OntologyManagement\$nugetId.nuspec")
			$test.PackPackage("c:\nuget\nuget.exe", "$tempDir\OntologyManagement\$nugetId.nuspec")
		}
		else 
		{
            if ($selfExtracting -eq $true) 
            {
			    $cmd = 'c:\Program Files\7-Zip\7z'
			    $zipFile = "$tempDir\OntologyManagement" + "\$project" + '_' + $version + '.exe'
        
			    $param = 'a', '-mmt' ,'-mx5', '-sfx7z.sfx', '-r', $zipFile, $destDir
			    #Write-Host $param
			    & $cmd $param
			    Write-Host "success!"
            }
		}
        
    }
}
    